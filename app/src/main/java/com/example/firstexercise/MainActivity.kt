package com.example.firstexercise

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    fun init() {
        val button = findViewById<Button>(R.id.button)
        val textView = findViewById<TextView>(R.id.textView)
        val editText = findViewById<EditText>(R.id.editText)
        button.setOnClickListener {
            textView.text = numberWithWords(editText.text.toString().toInt())
        }
    }

    fun numberWithWords(number: Int): String {
        val beforeTwenty = listOf<String>(
            "ნული", // 0
            "ერთი",
            "ორი",
            "სამი",
            "ოთხი",
            "ხუთი",
            "ექვსი",
            "შვიდი",
            "რვა",
            "ცხრა",
            "ათი",
            "თერთმეტი",
            "თორმეტი",
            "ცამეტი",
            "თოთხმეტი",
            "თხუთმეტი",
            "თექვსმეტი",
            "ჩვიდმეტი",
            "თვრამეტი",
            "ცხრამეტი",
            "ოცი"
        )
        val tens = listOf<String>(
            "",
            "ათი",
            "ოცი",
            "ოცდაათი",
            "ორმოცი",
            "ორმოცდაათი",
            "სამოცი",
            "სამოცდაათი",
            "ოთხმოცი",
            "ოცხმოცდაათი"
        )
        val prefixOfTens = listOf<String>(
            "",
            "",
            "ოცდა",
            "ოცდა",
            "ორმოცდა",
            "ორმოცდა",
            "სამოცდა",
            "სამოცდა",
            "ოთხმოცდა",
            "ოთხმოცდა"
        )
        val hundreds = listOf<String>(
            "",
            "ას",
            "ორას",
            "სამას",
            "ოთხას",
            "ხუთას",
            "ექვსას",
            "შვიდას",
            "რვაას",
            "ცხრაას",
            "ათას"
        )
        if (number < 0) return "enter number in the range [1,1000]"
        if (number < 20) return beforeTwenty[number]
        if (number < 100 && number % 10 == 0) return tens[number / 10]
        if (number <= 1000 && number % 100 == 0) return hundreds[number / 100] + "ი"
        if (number in 21..99) {
            val int = number / 10
            if (int % 2 == 0) {
                return prefixOfTens[int] + beforeTwenty[number % 10]
            } else return prefixOfTens[int] + beforeTwenty[number % 10 + 10]
        }
        if (number in 101..999) return hundreds[number / 100] + numberWithWords(number % 100)

        return "enter number in the range [1,1000]"
    }

}